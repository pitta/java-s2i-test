package ca.pitt.s2i.s2itest.value;

import java.util.Date;

/**
 * User class.
 * 
 */
public class User {
    
    /** Username. */
    private String username;
    
    /** Created date. */
    private Date createdDate;
    
    /**
     * Default constructor.
     */
    protected User() {
        // Empty.
    }
    
    /**
     * Main constructor.
     */
    public User(String username) {
        this.username = username;
        this.createdDate = new Date();
    }
    
    /**
     * Get username.
     * 
     * @return <code>String</code>
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * Get createdDate.
     * 
     * @return <code>Date</code>
     */
    public Date getCreatedDate() {
        return createdDate;
    }
    
}
