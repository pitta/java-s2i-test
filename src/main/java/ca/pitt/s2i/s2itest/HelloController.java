package ca.pitt.s2i.s2itest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ca.pitt.s2i.s2itest.value.User;

@RestController
@RequestMapping(path="/api", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
public class HelloController {
    
    @RequestMapping(method=RequestMethod.GET)
    public User getHello() {
        return new User("unknown");
    }
    
    @RequestMapping(method=RequestMethod.GET, params="name")
    public User getHelloWithParam(@RequestParam("name") String name) {
        return new User(name);
    }
    
}
