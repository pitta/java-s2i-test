package ca.pitt.s2i.s2itest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S2iTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(S2iTestApplication.class, args);
	}
}
